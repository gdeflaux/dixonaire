class DixonsController < ApplicationController

  def translate
    if params[:source]
      @dixon = Dixon.new
      @dixon.source = params[:source]
      @dixon.translate
    end
    
    respond_to do |format|
      format.html
      format.js
    end
  end
  
  def tts
    require 'open-uri'
    if params[:source] && params[:lang]
      ua = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/11.0.696.68 Safari/534.24"
      url = params[:source].to_url (params[:lang])
      content = open(url, "User-Agent" => ua).read
      send_data content
    end
  end
  
  def share
    DixonMailer.share_dixon(params[:email][:my_email], params[:email][:friend_email], params[:email][:message]).deliver
    respond_to do |format|
      format.js
    end
  end
  
end
