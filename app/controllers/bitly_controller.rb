class BitlyController < ApplicationController
	BITLY_USER = "dixonaire"
	BITLY_TOKEN = "R_f7ccf4f3df54cb209844a7790cf4bedf"
	
	require 'bitly'
	def shorten
		Bitly.use_api_version_3
		bitly = Bitly.new(BITLY_USER, BITLY_TOKEN)
		@url = bitly.shorten(params[:url])
		
		respond_to do |format|
			format.json { render :json => @url }
		end
	end
end