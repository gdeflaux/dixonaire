class Dixon < ActiveRecord::Base
  attr_accessible :code, :source, :translation
  
  def translate
    #self.translation = self.source.gsub(/[a-zA-Z]([a-zA-Z']+)/, 'd\1')
    utf_pattern = Regexp.new("\\p{Lower}".force_encoding("UTF-8"))
    self.translation = self.source.gsub(/([[:alpha:]])([[:alpha:]']+)/) do
      w = String.new $2
      $1.match(utf_pattern) ? "d#{w}" : "D#{w}"
    end
  end
  
end
