// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.ui.effect.all
//= require twitter/bootstrap
//= require_tree .

jQuery(function($){
	var timer;
	$('#dixon_source').keyup(function(e){
		// Disable tts support if more than 100 chars (Google restriction)
		if ($('#dixon_source').val().length > 100)
		{
			$('#tts-button').attr('disabled', 'disabled');
			$('#tts-button').removeAttr('data-toggle');
			$('#tts-button').attr('rel', 'tooltip');
			$('#tts-button').attr('data-placement', 'left');
			$('#tts-button').attr('data-original-title', 'Dixon drop dong : dax. 100 daractères');
			$('#tts-button').tooltip();
		}
		else
		{
			$('#tts-button').removeAttr('disabled');
			$('#tts-button').attr('data-toggle', 'dropdown');
			$('#tts-button').removeAttr('data-placement');
			$('#tts-button').removeAttr('rel');
			$('#tts-button').removeAttr('data-original-title');
		}
		
		// clear the timer as keys are still entered
		if(timer) clearTimeout(timer);

		// setup the timer
		timer = setTimeout(function(){
			$.get('/translate.js', { source: $("#dixon_source").val() });
		}, 1000);
	});
	
	$('#tts-button').click(function(){
		if ($('#dixon_source').val().length <= 0)
		{
			$('#dixon_source').effect('highlight');
			return false
		}
		return true
	});
	
	$('.play-button').click(function(){
		$('#player-source').attr('src', '/tts?lang=' + $(this).data('lang') + '&source=' + $('#dixon_translated').val());
		document.getElementById('player').load();
		document.getElementById('player').play();
	});
	
	$('#share-btn').click(function(){
		if ($('#dixon_source').val().length > 0)
		{
			var link = 'http://dixonaire.fr/translate?' + $.param({source: $('#dixon_source').val()})
			$.get('/shorten.json', { url: link}, function (data){
				var short_link
				short_link = data.short_url
				// Update Twitter button
	    		$('#twitter_tweet iframe').remove();
	    		var tweetBtn = $('<a></a>')
	        		.addClass('twitter-share-button')
	        		.attr('href', 'http://twitter.com/share')
	        		.attr('data-via', "DeDixonaire")
	        		.attr('data-lang', "fr")
	        		.attr('data-count', "none")
	        		.attr('data-url', short_link)
	        		.attr('data-text', 'Écoute ce Dixon !')
	        		.html("Dweeter");
	    		$('#twitter_tweet').append(tweetBtn);
	    		twttr.widgets.load();
				$('#email_message').html($('#email_blurb').val() + short_link);
			}, "json");
			return true;
		}
		else
		{
			$('#dixon_source').effect('highlight');
			return false;
		}
	});
	
	$('form').submit(function(){
		$('#email_my_email').attr('readonly', 'readonly');
		$('#email_friend_email').attr('readonly', 'readonly');
		$('#email_message').attr('readonly', 'readonly');
		$('#send-spinner').show('blind');
		return true;
	});
	
});