class CreateDixons < ActiveRecord::Migration
  def change
    create_table :dixons do |t|
      t.text :source
      t.text :translation
      t.string :code

      t.timestamps
    end
  end
end
